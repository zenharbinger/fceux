# FCEUX

Forked from [fceux](http://svn.code.sf.net/p/fceultra/code/fceu/trunk)

Added in a more fine-grain emulator speed increase/decrease.

## Requirements

```sh
sudo apt install libsdl1.2-dev libgtk2.0-dev scons
```

## Building
https://ubuntuforums.org/showthread.php?t=971455
```sh
sudo scons install
```
--  
Matt
